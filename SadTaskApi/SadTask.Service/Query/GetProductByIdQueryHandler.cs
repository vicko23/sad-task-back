﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SadTask.Data.Repository;
using SadTask.Domain;
using System.Threading;
using System.Threading.Tasks;

namespace SadTask.Service.Query
{
    public class GetProductByIdQueryHandler : IRequestHandler<GetProductByIdQuery, Product>
    {
        private readonly IRepository<Product> _repository;

        public GetProductByIdQueryHandler(IRepository<Product> repository)
        {
            _repository = repository;
        }

        public async Task<Product> Handle(GetProductByIdQuery request, CancellationToken cancellationToken)
        {
            return await _repository.GetAll().FirstOrDefaultAsync(p => p.Id == request.Id, cancellationToken);
        }
    }
}