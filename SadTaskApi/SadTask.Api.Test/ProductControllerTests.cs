using AutoMapper;
using FakeItEasy;
using FluentAssertions;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using SadTask.Domain;
using SadTask.Service.Query;
using SadTaskApi.Controllers;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using Xunit;

namespace SadTask.Api.Test
{
    public class ProductControllerTests
    {
        private readonly ProductController testController;
        private readonly IMediator _mediator;

        public ProductControllerTests()
        {
            _mediator = A.Fake<IMediator>();
            testController = new ProductController(A.Fake<IMapper>(), _mediator);

            var products = new[]
            {
                new Product
                {
                    Id = Guid.Parse("1ac55244-641c-45fc-836b-7861ea6f0b01"),
                    Title = "Notebook HP 7SH22EA",
                    Description = "HP Pavilion 15-bc513nm i7-9750H 15.6\"FHD AG 16GB 512GB PCIe GTX 1650 4GB FreeDOS",
                    Price = 1000,
                    Active = true
                },
                new Product
                {
                    Id = Guid.Parse("1011cffe-d2d0-466c-a6b4-8200d133a422"),
                    Title = "TV TESLA 40Q300BF",
                    Description = "Tesla DLED TV 40Q300BF",
                    Price = 150,
                    Active = false
                }
            };

            A.CallTo(() => _mediator.Send(A<GetActiveProductsQuery>._, A<CancellationToken>._))
                .Returns(products.Where(p => p.Active).ToList());
        }

        [Fact]
        public async void Products_ShouldReturnActiveOnes()
        {
            var result = await testController.GetActiveProducts();

            (result.Result as StatusCodeResult)?.StatusCode.Should().Be((int)HttpStatusCode.OK);
            result.Value.ForEach(p => p.Should().Be(true));
        }
    }
}