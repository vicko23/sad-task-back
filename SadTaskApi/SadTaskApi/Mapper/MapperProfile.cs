﻿using AutoMapper;
using SadTask.Domain;
using SadTaskApi.Mapper.Models;

namespace SadTaskApi.Mapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<ProductModel, Product>();
        }
    }
}