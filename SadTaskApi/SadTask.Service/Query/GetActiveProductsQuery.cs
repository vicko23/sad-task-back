﻿using MediatR;
using SadTask.Domain;
using System.Collections.Generic;

namespace SadTask.Service.Query
{
    public class GetActiveProductsQuery : IRequest<List<Product>>
    {
    }
}