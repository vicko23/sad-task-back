﻿using MediatR;
using SadTask.Domain;
using System;

namespace SadTask.Service.Query
{
    public class GetProductByIdQuery : IRequest<Product>
    {
        public Guid Id { get; set; }
    }
}