﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace SadTask.Data.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, new()
    {
        private readonly ShopContext _shopContext;

        public Repository(ShopContext shopContext)
        {
            _shopContext = shopContext;
        }

        public IQueryable<TEntity> GetAll()
        {
            return _shopContext.Set<TEntity>();
        }

        public async Task<TEntity> Create(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            await _shopContext.AddAsync(entity);
            await _shopContext.SaveChangesAsync();

            return entity;
        }

        public async Task<TEntity> Update(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            _shopContext.Update(entity);
            await _shopContext.SaveChangesAsync();

            return entity;
        }

        public async Task<bool> Delete(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException();
            }

            _shopContext.Remove(entity);
            await _shopContext.SaveChangesAsync();

            return true;
        }
    }
}