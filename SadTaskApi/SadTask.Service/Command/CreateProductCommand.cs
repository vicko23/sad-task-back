﻿using MediatR;
using SadTask.Domain;

namespace SadTask.Service.Command
{
    public class CreateProductCommand : IRequest<Product>
    {
        public Product Product { get; set; }
    }
}