﻿using MediatR;
using System;

namespace SadTask.Service.Services
{
    public class SampleService : ISampleService
    {
        private readonly IMediator _mediator;

        public SampleService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public void UpdateSomethingSomewhere(string modelForUpdate)
        {
            // TODO: update something
            // example: some related entities list when model is changed
            throw new NotImplementedException();
        }
    }
}