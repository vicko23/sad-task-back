﻿using MediatR;
using SadTask.Domain;

namespace SadTask.Service.Command
{
    public class UpdateProductCommand : IRequest<Product>
    {
        public Product Product { get; set; }
    }
}