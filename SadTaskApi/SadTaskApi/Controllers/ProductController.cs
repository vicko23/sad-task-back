﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SadTask.Domain;
using SadTask.Service.Command;
using SadTask.Service.Query;
using SadTaskApi.Mapper.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SadTaskApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public ProductController(IMapper mapper, IMediator mediator)
        {
            _mapper = mapper;
            _mediator = mediator;
        }

        /// <summary>
        /// Get all active products
        /// </summary>
        /// <returns>Returns the list of products</returns>
        [HttpGet]
        public async Task<ActionResult<List<Product>>> GetActiveProducts()
        {
            try
            {
                return await _mediator.Send(new GetActiveProductsQuery());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get product by Id
        /// </summary>
        /// <returns>Returns product</returns>
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<Product>> GetProductById(string id)
        {
            try
            {
                return await _mediator.Send(new GetProductByIdQuery {
                    Id = new Guid(id)
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Create new product in the database
        /// </summary>
        /// <returns>Returns the created product</returns>
        [HttpPost]
        [Authorize("write:product")]
        public async Task<ActionResult<Product>> Product([FromBody] ProductModel productModel)
        {
            try
            {
                return await _mediator.Send(new CreateProductCommand
                {
                    Product = _mapper.Map<Product>(productModel)
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}