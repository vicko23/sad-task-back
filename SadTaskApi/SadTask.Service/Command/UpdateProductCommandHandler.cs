﻿using MediatR;
using SadTask.Data.Repository;
using SadTask.Domain;
using SadTask.Message.Send.Sender;
using System.Threading;
using System.Threading.Tasks;

namespace SadTask.Service.Command
{
    public class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand, Product>
    {
        private readonly IRepository<Product> _repository;
        private readonly IProductUpdateSender _productUpdateSender;

        public UpdateProductCommandHandler(IRepository<Product> repository, IProductUpdateSender productUpdateSender)
        {
            _repository = repository;
            _productUpdateSender = productUpdateSender;
        }

        public async Task<Product> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
        {
            var product = await _repository.Update(request.Product);

            _productUpdateSender.SendProduct(product);

            return product;
        }
    }
}