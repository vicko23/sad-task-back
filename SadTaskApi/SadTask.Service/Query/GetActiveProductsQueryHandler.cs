﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using SadTask.Data.Repository;
using SadTask.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SadTask.Service.Query
{
    public class GetActiveProductsQueryHandler : IRequestHandler<GetActiveProductsQuery, List<Product>>
    {
        private readonly IRepository<Product> _repository;

        public GetActiveProductsQueryHandler(IRepository<Product> repository)
        {
            _repository = repository;
        }

        public async Task<List<Product>> Handle(GetActiveProductsQuery request, CancellationToken cancellationToken)
        {
            return await _repository.GetAll().Where(p => p.Active).ToListAsync(cancellationToken);
        }
    }
}