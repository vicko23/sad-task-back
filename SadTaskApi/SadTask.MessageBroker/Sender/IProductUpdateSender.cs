﻿using SadTask.Domain;

namespace SadTask.Message.Send.Sender
{
    public interface IProductUpdateSender
    {
        void SendProduct(Product product);
    }
}