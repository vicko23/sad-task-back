﻿using MediatR;
using SadTask.Data.Repository;
using SadTask.Domain;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SadTask.Service.Command
{
    public class CreateProductCommandHandler : IRequestHandler<CreateProductCommand, Product>
    {
        private readonly IRepository<Product> _repository;

        public CreateProductCommandHandler(IRepository<Product> repository)
        {
            _repository = repository;
        }

        public async Task<Product> Handle(CreateProductCommand request, CancellationToken cancellationToken)
        {
            request.Product.Id = Guid.NewGuid();
            return await _repository.Create(request.Product);
        }
    }
}