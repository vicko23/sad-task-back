﻿using System.Linq;
using System.Threading.Tasks;

namespace SadTask.Data.Repository
{
    public interface IRepository<TEntity> where TEntity : class, new()
    {
        IQueryable<TEntity> GetAll();

        Task<TEntity> Create(TEntity entity);

        Task<TEntity> Update(TEntity entity);

        Task<bool> Delete(TEntity entity);
    }
}