﻿using Microsoft.EntityFrameworkCore;
using SadTask.Domain;
using System;

namespace SadTask.Data
{
    public class ShopContext : DbContext
    {
        public ShopContext()
        {
        }

        public ShopContext(DbContextOptions<ShopContext> options)
            : base(options)
        {
            var products = new[]
            {
                new Product
                {
                    Id = Guid.Parse("1ac55244-641c-45fc-836b-7861ea6f0b01"),
                    Title = "Notebook HP 7SH22EA",
                    Description = "HP Pavilion 15-bc513nm i7-9750H 15.6\"FHD AG 16GB 512GB PCIe GTX 1650 4GB FreeDOS",
                    Price = 1000,
                    Active = true
                },
                new Product
                {
                    Id = Guid.Parse("1011cffe-d2d0-466c-a6b4-8200d133a422"),
                    Title = "TV TESLA 40Q300BF",
                    Description = "Tesla DLED TV 40Q300BF",
                    Price = 150,
                    Active = false
                },
                new Product
                {
                    Id = Guid.Parse("1233209f-0a21-41b3-be40-9f2ca0e0010e"),
                    Title = "Apple iPhone 11",
                    Description = "Apple iPhone 11 iOS 13 64GB/128GB/256GB storage, no card slot",
                    Price = 600,
                    Active = true
                }
            };

            Products.AddRange(products);
            SaveChanges();
        }

        public virtual DbSet<Product> Products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Title).IsRequired();
            });
        }
    }
}